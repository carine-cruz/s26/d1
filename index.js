//Session26 - NODEJS

console.log(`Welcome to Node.js Session!`);

//HTTP
    // The HTTP module provides a way of making Node.js transfer data over HTTP (Hyper Text Transfer Protocol)

    //we use require() directive keyword to import a module
    
    const HTTP = require("http");

    //Creates an HTTP server using createServer(requestListener()) method
    HTTP.createServer((request,response)=>{
        
        if(request.url === "/profile"){
            response.writeHead(400, {"Content-Type": "text/plain"});
            response.write("Bad Request");
            response.end();
        } else if( request.url === "/greeting"){
            response.writeHead(200, {"Content-Type": "text/plain"});
            response.write("This is from the homepage.");
            response.end();
        } else{
            response.writeHead(200, {"Content-Type": "text/plain"});
            response.write("This is the response from the request.");
            response.end();
        }

    }).listen(3000)
